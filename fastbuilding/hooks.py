# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "fastbuilding"
app_title = "Fast Building cContractors"
app_publisher = "fayez qandeel"
app_description = "Contractors"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "customvivvo@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/fastbuilding/css/fastbuilding.css"
# app_include_js = "/assets/fastbuilding/js/fastbuilding.js"

# include js, css files in header of web template
# web_include_css = "/assets/fastbuilding/css/fastbuilding.css"
# web_include_js = "/assets/fastbuilding/js/fastbuilding.js"

# Home Pages
# ----------
app_include_css = [
	"/assets/fastbuilding/css/custom.css"
]
# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "fastbuilding.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "fastbuilding.install.before_install"
# after_install = "fastbuilding.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "fastbuilding.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"fastbuilding.tasks.all"
# 	],
# 	"daily": [
# 		"fastbuilding.tasks.daily"
# 	],
# 	"hourly": [
# 		"fastbuilding.tasks.hourly"
# 	],
# 	"weekly": [
# 		"fastbuilding.tasks.weekly"
# 	]
# 	"monthly": [
# 		"fastbuilding.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "fastbuilding.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "fastbuilding.event.get_events"
# }

